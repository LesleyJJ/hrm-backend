const mongoose = require('mongoose');
const candidateApplicationSchema = new mongoose.Schema({
    Vacancy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Vacancy',
        required: true
    },
    applicantFirstName: {
        type: String,
        required: true
    },
    applicantMiddleName: {
        type: String
    },
    applicantLastName: {
        type: String,
        required: true
    },
    applicantEmail: {
        type: String,
        required: true
    },
    applicantMobileNumber:{
        type: String
    },
    resume: {
        file: Buffer,
        fileName: String
    },
    keyWords: [String],
    comment: String,
    applicationDate: {
        type: Date,
        default: Date.now,
        required: true
    },
    applicationMethod: {
        type: String,
        enum: ['online', 'manual'],
        required: true
    },
    status: {
        type: String,
        required: true,
        enum: ['application-initiated', 'short-listed', 'interview-scheduled', 'interview-passed', 'interview-failed', 'job-offered', 'offer-declined', 'rejected', 'hired']
    },
    history: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'RecruitmentProcess'
    }]
});
const CandidateApplication = mongoose.model('CandidateApplication', candidateApplicationSchema);
module.exports = {candidateApplicationSchema, CandidateApplication};