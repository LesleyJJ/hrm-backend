const mongoose = require('mongoose');
const {interviewSchema} = require('../InterviewComponent/interviewModel');
const recruitmentProcessSchema = new mongoose.Schema({
    currentStatus: {
        type: String,
        enum: ['application-initiated', 'short-listed', 'interview-scheduled', 'interview-passed', 'interview-failed', 'job-offered', 'offer-declined']
    },
    performedAction: {
        type: String,
        required: true,
        enum: ['application-initiated', 'short-listed', 'interview-scheduled', 'interview-passed', 'interview-failed', 'job-offered', 'offer-declined', 'rejected', 'hired']
    },
    performedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee',
        required: true
    },
    performedDate: {
        type: Date,
        default: Date.now,
        required: true
    },
    scheduledInterview: {
        type: interviewSchema
    },
    offeredJobApprovedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    },
    attachment: {
        file: Buffer,
        fileName: String
    },
    notes: String
});
const RecruitmentProcess = mongoose.model('RecruitmentProcess', recruitmentProcessSchema);
module.exports = {recruitmentProcessSchema, RecruitmentProcess}