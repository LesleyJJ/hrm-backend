const mongoose = require('mongoose');
const Int32 = require('mongoose-int32');
const interviewSchema = new mongoose.Schema({
    interviewTitle: {
        type: String,
        required: true
    },
    interviewers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }],
    scheduledDate: {
        type: Date,
        required: true
    },
    scheduledTime: {
        type: Int32,
        min: 0,
        max: 86399
    },
});
const Interview = mongoose.model('Interview', interviewSchema);
module.exports = {interviewSchema, Interview};