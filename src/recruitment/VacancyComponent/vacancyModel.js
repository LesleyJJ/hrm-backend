const mongoose = require('mongoose');
const vacancySchema = new mongoose.Schema({
    jobTitle: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'JobTitle',
        required: true
    },
    vacancyName: {
        type: String,
        required: true
    },
    hiringManager: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee',
        required: true
    },
    positionNumber: {
        type: Number,
        required: true,
        default: 1
    },
    description: {
        type: String
    },
    active: Boolean,
    publish: Boolean
});
const Vacancy = mongoose.model('Vacancy', vacancySchema);
module.exports = {vacancySchema, Vacancy};