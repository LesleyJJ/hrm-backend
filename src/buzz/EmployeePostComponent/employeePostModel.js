const mongoose = require('mongoose');
const employeePostSchema = new mongoose.Schema({
    postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee',
        required: true
    },
    status: {
        type: String
    },
    image: {
        about: String,
        file: Buffer,
        fileName: String
    },
    video: {
        type: String
    },
    sharedPost: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'EmployeePost'
    },
    postedTime: {
        type: Date,
        default: Date.now,
        required: true
    },
    likes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }],
    disLikes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }],
    shares: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }],
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PostComment'
    }]
});
const EmployeePost = mongoose.model('EmployeePost', employeePostSchema);
module.exports = {employeePostSchema, EmployeePost};