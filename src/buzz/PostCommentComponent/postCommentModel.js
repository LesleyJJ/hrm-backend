const mongoose = require('mongoose');
const postCommentSchema = new mongoose.Schema({
    commentedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee',
        required: true
    },
    content: {
        type: String,
        required: true
    },
    commentedTime: {
        type: Date,
        default: Date.now,
        required: true
    },
    likes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }],
    disLikes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }]
});
const PostComment = mongoose.model('PostComment', postCommentSchema);
module.exports = {PostComment, postCommentSchema};